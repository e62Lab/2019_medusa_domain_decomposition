## Domain decomposition Medusa
This repository contains a rudimentary implementation of domain decomposition for PDE's with the Medusa library. The examples include prood of concept for additive and multiplicative overlapping Schwartz methods. The Poisson equation is solved on two overlapping rectangles and two overlapping circles, scripts for convergence of solutions are available in the */process* directory.

Clear next steps are:
1. Generalizing the procedure for more subdomains than just two.
2. Faster implementation, preferably parallel.
3. Testing different approaches of estimating values on the subdomain edges (so far,  the value was taken to be that of the value in the nearest node, or an approximation is constructed in meshless fashion from a few nearest neighbors)
4. How to treat irregular domains, i.e. what sort of tilings make sense for domain decompositions

Articles and other material that may be of use:
* [An introduction to Schwartz methods](https://calcul.math.cnrs.fr/attachments/spip/Documents/Ecoles/ET2011DD/VDolean1.pdf)
* [Domain Decomposition for Radial Basis Meshless Methods](https://onlinelibrary.wiley.com/doi/abs/10.1002/num.10096)
* [Domain Decomposition Methods for the Numerical Solution of Partial Differential Equations](https://www.springer.com/gp/book/9783540772057)
* [An Introduction to Domain Decomposition Methods: algorithms, theory and parallel implementation](https://hal.archives-ouvertes.fr/cel-01100932v4/document)
* [Overlaping Domain decomposition methods](http://inutech.de/res/doc/Diffpack/Reports/dd-chap.pdf)
