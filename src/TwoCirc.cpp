//
// Created by blaz on 18.10.2019.
// Poisson in two dimensions solved on two domains, which are filled with scatter plot. The values in the other domain



#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include "medusa/bits/approximations/WLS.hpp"
#include <string>
#include <cmath>


using namespace mm;
int main(){

    double dx = 0.01;
    // number of nodes used in wls approximation other domain value
    int na = 10;
    double radius = 0.5;
    int iterations = 30;
    int n = 9; // support size


    Timer timer;
    timer.addCheckPoint("Begin");

    // Construct domain A and B, in this case they are two offset squares
    BallShape<Vec2d> A(0.0, radius);
    BallShape<Vec2d> B({0.75, 0.0}, radius);

    // Construct General Fill
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);

    // Construct Domain discretizaitons
    DomainDiscretization<Vec2d> domainA = A.discretizeBoundaryWithStep(dx);
    DomainDiscretization<Vec2d> domainB = B.discretizeBoundaryWithStep(dx);

    // Obtain boundary nodes in neighboring domains (indices)
    // This is done at this point, because there are only boundary nodes in both A in B at this point
    auto boundAinB = domainA.positions().filter([=](const Vec2d& p) { return B.contains(p);});
    auto boundBinA = domainB.positions().filter([=](const Vec2d& p) { return A.contains(p);});

    // Fill the inside of domains
    domainA.fill(fill_engine, dx);
    domainB.fill(fill_engine, dx);

    // Construct KDTrees for point lookup when adjusting boundary conditions
    KDTree<Vec2d> treeA(domainA.positions());
    KDTree<Vec2d> treeB(domainB.positions());

    // Output data
    std::string name = "../../postprocess/process/Data/TwoCirc-WLS.h5";
    HDF out(name, HDF::DESTROY);
    out.writeDomain("domainA", domainA);
    out.writeDomain("domainB", domainB);
    out.writeIntAttribute("iterations", iterations);

    int Na = domainA.size();
    int Nb = domainB.size();

    // find support
    domainA.findSupport(FindClosest(n));
    domainB.findSupport(FindClosest(n));

    // construct engine for weights
    WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest> wls({n, 30});

    // construct engine for value approximation
    WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest> appr(2, 100.0);

    // vector to store support nodes for above approximation
    Range<Vec2d> appsupport(na);
    Eigen::VectorXd appvals(na);

    // construct weight matrices and both rhsides
    Eigen::SparseMatrix<double, Eigen::RowMajor> Ma(Na, Na);
    Eigen::SparseMatrix<double, Eigen::RowMajor> Mb(Nb, Nb);
    Eigen::VectorXd rhs_a(Na);
    Eigen::VectorXd rhs_b(Nb);

    // shapes
    auto storageA = domainA.computeShapes<sh::lap>(wls);
    auto storageB = domainB.computeShapes<sh::lap>(wls);

    // operators
    auto op_A = storageA.implicitOperators(Ma, rhs_a);
    auto op_B = storageB.implicitOperators(Mb, rhs_b);

    // solvers
    Eigen::BiCGSTAB<decltype(Ma), Eigen::IncompleteLUT<double>> solverA;
    Eigen::BiCGSTAB<decltype(Mb), Eigen::IncompleteLUT<double>> solverB;

    // fill the matrices
    for (int i : domainA.interior()){
        auto pos = domainA.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_A.lap(i) = -std::exp(-x+2*y)*(std::sin(x)*(4*std::sin(y)-3*std::cos(y))+2*std::cos(y)*std::cos(x));

    }
    // set boundary condition, this will be for fixed every iteration
    for (int i : domainA.boundary()){
        auto pos = domainA.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_A.value(i) = std::exp(-x+2*y)*std::sin(x)*std::cos(y);
    }
    for (int i : boundAinB){
        // We have to be careful when setting these values in matrix M, as we will be changing the RHS a later to fix BC
        Ma.coeffRef(i, i) = 1.0; // to make this easier we will make all of the coeffs ones
        rhs_a(i) = 1.0; // Because in real use case, we do not know this boundary value exactly!
    }

    for (int i : domainB.interior()){
        auto pos = domainB.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_B.lap(i) = -std::exp(-x+2*y)*(std::sin(x)*(4*std::sin(y)-3*std::cos(y))+2*std::cos(y)*std::cos(x));
    }
    for (int i : domainB.boundary()){
        auto pos = domainB.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_B.value(i) = std::exp(-x+2*y)*std::sin(x)*std::cos(y);
    }
    for (int i : boundBinA) {
        Mb.coeffRef(i, i) = 1.0;
    }

    // compute matrices
    solverA.compute(Ma);
    solverB.compute(Mb);

    // init complete
    timer.addCheckPoint("Init");

    // Intermediate output
//    out.writeSparseMatrix("Ma", Ma);
//    out.writeSparseMatrix("Mb", Mb);
//    out.writeIntArray("boundAinB", boundAinB);
//    out.writeIntArray("boundBinA", boundBinA);

    // start iterating
    std::cout << "Iteration no. " << 0 << "\n";
    std::cout << "Solving domain A" << "\n";

    // solve equation on subdomain A, with initial border
    ScalarFieldd u_a = solverA.solve(rhs_a);

    timer.addCheckPoint("");

    // fix boundary condition on B, the left boundary values are set to approximation from u_a
    std::cout << "Adjusting boundary condition for domain B" << "\n";
    for (int i : boundBinA){
        Vec2d pos_b = domainB.pos(i);
        // find nearest in domain A
        auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
        auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this works

        for (int j = 0; j < na; j++){
            appsupport[j] = domainA.pos(ia[j]);
            appvals[j] = u_a(ia[j]);
        }
        std::cout << "Node being approx at: " << pos_b << std::endl;
        std::cout << "distances: " << r2a << std::endl;
        auto approximant = appr.getApproximant(pos_b, appsupport, appvals);
        // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
        rhs_b(i) = approximant(pos_b);
//        rhs_b(i) = appvals[0];
        std::cout << "values: " << appvals << std::endl;
        std::cout << "value (approx): " << approximant(pos_b) << std::endl;
        std::cout << "----------------------------------------" << std::endl;
    }

    // solve B
    std::cout << "Solving domain B" << "\n\n";

    ScalarFieldd u_b = solverB.solve(rhs_b);
    // write solution
    out.writeDoubleArray("solA-0", u_a);
    out.writeDoubleArray("solB-0", u_b);

    for(int iter = 1; iter < iterations; iter++){
        std::cout << "Iteration no. " << iter << "\n";
        std::cout << "Adjusting boundary condition for domain A" << "\n";
        std::cout << "Solving domain A" << "\n";
        for (int i : boundAinB){
            Vec2d pos_a = domainA.pos(i);
            // find nearest in domain A
            auto nearest_in_B = treeB.query(pos_a, na); // std::tie of Range<int> and Range<double>
            auto [ib, r2b] = nearest_in_B; // structured binding declaration, todo learn how this works

            for (int j = 0; j < na; j++){
                appsupport[j] = domainB.pos(ib[j]);
                appvals[j] = u_b(ib[j]);
            }
            std::cout << "Node being approx at: " << pos_a << std::endl;
            std::cout << "distances: " << r2b<< std::endl;
            auto approximant = appr.getApproximant(pos_a, appsupport, appvals);
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
            //        rhs_b(i) = approximant(pos_b);
            std::cout << "values: " << appvals << std::endl;
            std::cout << "value (approx): " << approximant(pos_a) << std::endl;
            std::cout << "----------------------------------------" << std::endl;
            rhs_a(i) = approximant(pos_a);
//            rhs_a(i) = appvals[0];
        }

        u_a = solverA.solve(rhs_a);

        std::string nameA = "solA-" + std::to_string(iter);
        std::string nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);

        std::cout << "Adjusting boundary condition for domain B" << "\n";
        std::cout << "Solving domain B" << "\n\n";

        for (int i : boundBinA){
            Vec2d pos_b = domainB.pos(i);
            // find nearest in domain A
            auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
            auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo: learn how this work

            for (int j = 0; j < na; j++){
                appsupport[j] = domainA.pos(ia[j]);
                appvals[j] = u_a(ia[j]);
            }
            std::cout << "Node being approx at: " << pos_b << std::endl;
            std::cout << "distances: " << r2a << std::endl;
            auto approximant = appr.getApproximant(pos_b, appsupport, appvals);
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
    //        rhs_b(i) = approximant(pos_b);
            std::cout << "values: " << appvals << std::endl;
            std::cout << "value (approx): " << approximant(pos_b) << std::endl;
            std::cout << "----------------------------------------" << std::endl;
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
            rhs_b(i) = approximant(pos_b);
//            rhs_b(i) = appvals[0];
        }
        u_b = solverB.solve(rhs_b);

        iter++;
        // write solution
        nameA = "solA-" + std::to_string(iter);
        nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);
        }
    out.close();

    return 0;
}