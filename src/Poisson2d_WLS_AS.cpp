//
// Created by blaz on 18.10.2019.
// Poisson in two dimensions solved on two domains, which are filled with scatter plot. The walues in the other domain
// are constructed with WLS approximaton, from nearest couple of points in the second domain.

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include "medusa/bits/approximations/WLS.hpp"

using namespace mm;
int main(){

    double dx = 0.01;
    // number of nodes used in wls approximation other domain value
    int na = 4;


    // Construct domain A and B, in this case they are two offset squares
    BoxShape<Vec2d> A(0.0, {0.7, 1.0});
    BoxShape<Vec2d> B({0.3, 0.0}, {1.0, 1.0});

    // Construct General Fill
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);

    // Construct Domain discretizaitons
    DomainDiscretization<Vec2d> domainA = A.discretizeBoundaryWithStep(dx);
    DomainDiscretization<Vec2d> domainB = B.discretizeBoundaryWithStep(dx);

    domainA.fill(fill_engine, dx);
    domainB.fill(fill_engine, dx);


    // Construct KDTrees for point lookup when adjusting boundary conditions
    KDTree<Vec2d> treeA(domainA.positions());
    KDTree<Vec2d> treeB(domainB.positions());

    // Output data
    HDF out("../../postprocess/plot/Data/Poisson2d_AS-WLS.h5", HDF::DESTROY);
    out.writeDomain("domainA", domainA);
    out.writeDomain("domainB", domainB);

    int Na = domainA.size();
    int Nb = domainB.size();

    int n = 9; // support size

    // find support
    domainA.findSupport(FindClosest(n));
    domainB.findSupport(FindClosest(n));

    // construct engine for weights
    WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest> wls({n, 30});

    // construct engine for value approximation
    WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest> appr(1, 1.0);

    // vector to store support nodes for above approximation
    Range<Vec2d> appsupport(na); // not counting the center point
    Eigen::VectorXd appvals(na); // all values?


    // construct weight matrices and both rhsides
    Eigen::SparseMatrix<double, Eigen::RowMajor> Ma(Na, Na);
    Eigen::SparseMatrix<double, Eigen::RowMajor> Mb(Nb, Nb);
    Eigen::VectorXd rhs_a(Na);
    Eigen::VectorXd rhs_b(Nb);

    // shapes
    auto storageA = domainA.computeShapes<sh::lap>(wls);
    auto storageB = domainB.computeShapes<sh::lap>(wls);

    // operators
    auto op_A = storageA.implicitOperators(Ma, rhs_a);
    auto op_B = storageB.implicitOperators(Mb, rhs_b);

    // solvers
    Eigen::BiCGSTAB<decltype(Ma), Eigen::IncompleteLUT<double>> solverA;
    Eigen::BiCGSTAB<decltype(Mb), Eigen::IncompleteLUT<double>> solverB;


    // WLS for approximation of values from other domain

    // case => lap u = -2*PI*sin(PI x)*sin(PI*y)
    // fill the matrices, lap u = 1
    for (int i : domainA.interior()){
        auto pos = domainA.pos(i);
        op_A.lap(i) = -2*PI*PI*std::sin(PI*pos[0])*std::sin(PI*pos[1]);
    }
    // set boundary condition, this will be fixed every iteration
    for (int i : domainA.boundary()){
        op_A.value(i) = 0;
    }

    for (int i : domainB.interior()){
        auto pos = domainB.pos(i);
        op_B.lap(i) = -2*PI*PI*std::sin(PI*pos[0])*std::sin(PI*pos[1]);
    }
    for (int i : domainB.boundary()){
        op_B.value(i) = 0;
    }

    // compute matrices
    solverA.compute(Ma);
    solverB.compute(Mb);


    int iterations = 40;
    std::cout << "Iteration no. " << 0 << "\n";
    std::cout << "Solving domain A" << "\n";

    // solve equation on subdomain A, with initial border
    ScalarFieldd u_a = solverA.solve(rhs_a);

    // Vector to save distances into, for analysis, for border node we want na spaces in vector
//    int nbord = (domainB.types() == -1).size();
//    Eigen::VectorXd dist(na*(domainB.types() == -1).size());
//    std::cout << dist.size() << std::endl;


    // fix boundary condition on B, the left boundary values are set to approximation from u_a
    std::cout << "Adjusting boundary condition for domain B" << "\n";
    for (int i : domainB.types() == -1){
        Vec2d pos_b = domainB.pos(i);
        // find nearest in domain A
        auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
        auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this works

        for (int j = 0; j < na; j++){
            appsupport[j] = domainA.pos(ia[j]);
            appvals[j] = u_a(ia[j]);
        }

        auto approximant = appr.getApproximant(pos_b, appsupport, appvals);
        // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
        rhs_b(i) = approximant(pos_b);
    }

    // solve B
    std::cout << "Solving domain B" << "\n\n";

    ScalarFieldd u_b = solverB.solve(rhs_b);
    // write solution
    out.writeDoubleArray("solA-0", u_a);
    out.writeDoubleArray("solB-0", u_b);
//    out.writeDoubleArray("dist", dist);

//     iterate
    for(int iter = 1; iter < iterations; iter++){
        std::cout << "Iteration no. " << iter << "\n";
        std::cout << "Adjusting boundary condition for domain A" << "\n";
        std::cout << "Solving domain A" << "\n";
        for (int i : domainA.types() == -2){
            Vec2d pos_a = domainA.pos(i);
            // find nearest in domain A
            auto nearest_in_B = treeB.query(pos_a, na); // std::tie of Range<int> and Range<double>
            auto [ib, r2b] = nearest_in_B; // structured binding declaration, todo learn how this works

            for (int j = 0; j < na; j++){
                appsupport[j] = domainB.pos(ib[j]);
                appvals[j] = u_b(ib[j]);
            }

            auto approximant = appr.getApproximant(pos_a, appsupport, appvals);

            rhs_a(i) = approximant(pos_a);
        }
        u_a = solverA.solve(rhs_a);

        std::string nameA = "solA-" + std::to_string(iter);
        std::string nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);

        std::cout << "Adjusting boundary condition for domain B" << "\n";
        std::cout << "Solving domain B" << "\n\n";

        for (int i : domainB.types() == -1){
            Vec2d pos_b = domainB.pos(i);
            // find nearest in domain A
            auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
            auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this work

            for (int j = 0; j < na; j++){
                appsupport[j] = domainA.pos(ia[j]);

                appvals[j] = u_a(ia[j]);
            }

            auto approximant = appr.getApproximant(pos_b, appsupport, appvals);
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
            rhs_b(i) = approximant(pos_b);
        }

        u_b = solverB.solve(rhs_b);

        iter++;

        // write solution
        nameA = "solA-" + std::to_string(iter);
        nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);
    }

    out.close();

    return 0;
}



