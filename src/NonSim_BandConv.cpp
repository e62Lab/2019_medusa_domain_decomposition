//
// Created by blaz on 18.10.2019.
// Poisson in two dimensions solved on two domains, which are filled with scatter plot. The values in the other domain
// are constructed with WLS approximaton, from nearest couple of points in the second domain. Checking convergence depencance
// on overlapping band
// This case is no longer symmetric

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include "medusa/bits/approximations/WLS.hpp"
#include <string>
#include <cmath>


using namespace mm;
int main(){

    double dx = 0.01;
    // number of nodes used in wls approximation other domain value
    int na = 4;
    Range<double> bands = {0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45};
    double squarewidth = 1.0;
    int iterations = 40;
    int n = 9; // support size

    // solve problem for different bandwidths
    for (double width : bands){
        std::cout << "Solving for, width = "<< width << std::endl;

        // Construct domain A and B, in this case they are two offset squares
        BoxShape<Vec2d> A(0.0, {squarewidth/2.0 + width, squarewidth});
        BoxShape<Vec2d> B({squarewidth/2.0 - width, 0.0}, {squarewidth, squarewidth});

        // Construct General Fill
        GeneralFill<Vec2d> fill_engine;
        fill_engine.seed(1);

        // Construct Domain discretizaitons
        DomainDiscretization<Vec2d> domainA = A.discretizeBoundaryWithStep(dx);
        DomainDiscretization<Vec2d> domainB = B.discretizeBoundaryWithStep(dx);

        domainA.fill(fill_engine, dx);
        domainB.fill(fill_engine, dx);


        // Construct KDTrees for point lookup when adjusting boundary conditions
        KDTree<Vec2d> treeA(domainA.positions());
        KDTree<Vec2d> treeB(domainB.positions());

        // Output data
        std::string name = "../../postprocess/process/Data/NonSim-WLS-" + std::to_string((int) std::round(100*width)) + ".h5";
        HDF out(name, HDF::DESTROY);
        out.writeDomain("domainA", domainA);
        out.writeDomain("domainB", domainB);
        out.writeIntAttribute("iterations", iterations);

        int Na = domainA.size();
        int Nb = domainB.size();

        // find support
        domainA.findSupport(FindClosest(n));
        domainB.findSupport(FindClosest(n));

        // construct engine for weights
        WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest> wls({n, 30});

        // construct engine for value approximation
        WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest> appr(1, 1.0);

        // vector to store support nodes for above approximation
        Range<Vec2d> appsupport(na);
        Eigen::VectorXd appvals(na);

        // construct weight matrices and both rhsides
        Eigen::SparseMatrix<double, Eigen::RowMajor> Ma(Na, Na);
        Eigen::SparseMatrix<double, Eigen::RowMajor> Mb(Nb, Nb);
        Eigen::VectorXd rhs_a(Na);
        Eigen::VectorXd rhs_b(Nb);

        // shapes
        auto storageA = domainA.computeShapes<sh::lap>(wls);
        auto storageB = domainB.computeShapes<sh::lap>(wls);

        // operators
        auto op_A = storageA.implicitOperators(Ma, rhs_a);
        auto op_B = storageB.implicitOperators(Mb, rhs_b);

        // solvers
        Eigen::BiCGSTAB<decltype(Ma), Eigen::IncompleteLUT<double>> solverA;
        Eigen::BiCGSTAB<decltype(Mb), Eigen::IncompleteLUT<double>> solverB;

        // fill the matrices
        for (int i : domainA.interior()){
            auto pos = domainA.pos(i);
            double x = pos[0];
            double y = pos[1];
            op_A.lap(i) = -std::sin(3/2*PI*x)*std::exp(-x-y)*((4*PI*PI-1)*std::sin(2*PI*y)+4*PI*std::cos(2*PI*y))
                    -1/4*std::sin(2*PI*y)*std::exp(-x-y)*((9*PI*PI-4)*std::sin(3/2*PI*x)+12*PI*std::cos(3/2*PI*x));
        }
        // set boundary condition, this will be fixed every iteration
        for (int i : domainA.types() == -1){
            op_A.value(i) = 0;
        }
        for (int i : domainA.types() == -2){
            op_A.value(i) = 0; // be careful, this one (should?) be set to zero, this is not the real boudary condition of the problem
        }
        for (int i : domainA.types() == -3){
            auto pos = domainA.pos(i);
            op_A.value(i) = pos[0];
        }
        for (int i : domainA.types() == -4){
            auto pos = domainA.pos(i);
            op_A.value(i) = pos[0];
        }

        for (int i : domainB.interior()){
            auto pos = domainB.pos(i);
            double x = pos[0];
            double y = pos[1];
            op_B.lap(i) = -std::sin(3/2*PI*x)*std::exp(-x-y)*((4*PI*PI-1)*std::sin(2*PI*y)+4*PI*std::cos(2*PI*y))
                          -1/4*std::sin(2*PI*y)*std::exp(-x-y)*((9*PI*PI-4)*std::sin(3/2*PI*x)+12*PI*std::cos(3/2*PI*x));
        }
        for (int i : domainB.types() == -1){
            op_B.value(i) = 0;
        }
        for (int i : domainB.types() == -2){
            op_B.value(i) = 1; // the real right boundary of the domain
        }
        for (int i : domainB.types() == -3){
            auto pos = domainB.pos(i);
            op_B.value(i) = pos[0];
        }
        for (int i : domainB.types() == -4){
            auto pos = domainB.pos(i);
            op_B.value(i) = pos[0];
        }

        // compute matrices
        solverA.compute(Ma);
        solverB.compute(Mb);

        // start iterating
        std::cout << "Iteration no. " << 0 << "\n";
        std::cout << "Solving domain A" << "\n";

        // solve equation on subdomain A, with initial border
        ScalarFieldd u_a = solverA.solve(rhs_a);

        // fix boundary condition on B, the left boundary values are set to approximation from u_a
        std::cout << "Adjusting boundary condition for domain B" << "\n";
        for (int i : domainB.types() == -1){
            Vec2d pos_b = domainB.pos(i);
            // find nearest in domain A
            auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
            auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this works

            for (int j = 0; j < na; j++){
                appsupport[j] = domainA.pos(ia[j]);
                appvals[j] = u_a(ia[j]);
            }

            auto approximant = appr.getApproximant(pos_b, appsupport, appvals);
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
            rhs_b(i) = approximant(pos_b);
        }

        // solve B
        std::cout << "Solving domain B" << "\n\n";

        ScalarFieldd u_b = solverB.solve(rhs_b);
        // write solution
        out.writeDoubleArray("solA-0", u_a);
        out.writeDoubleArray("solB-0", u_b);

        for(int iter = 1; iter < iterations; iter++){
            std::cout << "Iteration no. " << iter << "\n";
            std::cout << "Adjusting boundary condition for domain A" << "\n";
            std::cout << "Solving domain A" << "\n";
            for (int i : domainA.types() == -2){
                Vec2d pos_a = domainA.pos(i);
                // find nearest in domain A
                auto nearest_in_B = treeB.query(pos_a, na); // std::tie of Range<int> and Range<double>
                auto [ib, r2b] = nearest_in_B; // structured binding declaration, todo learn how this works

                for (int j = 0; j < na; j++){
                    appsupport[j] = domainB.pos(ib[j]);
                    appvals[j] = u_b(ib[j]);
                }
                auto approximant = appr.getApproximant(pos_a, appsupport, appvals);
                rhs_a(i) = approximant(pos_a);
            }
            u_a = solverA.solve(rhs_a);

            std::string nameA = "solA-" + std::to_string(iter);
            std::string nameB = "solB-" + std::to_string(iter);
            out.writeDoubleArray(nameA, u_a);
            out.writeDoubleArray(nameB, u_b);

            std::cout << "Adjusting boundary condition for domain B" << "\n";
            std::cout << "Solving domain B" << "\n\n";

            for (int i : domainB.types() == -1){
                Vec2d pos_b = domainB.pos(i);
                // find nearest in domain A
                auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
                auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo: learn how this work

                for (int j = 0; j < na; j++){
                    appsupport[j] = domainA.pos(ia[j]);
                    appvals[j] = u_a(ia[j]);
                }
                auto approximant = appr.getApproximant(pos_b, appsupport, appvals);
                // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
                rhs_b(i) = approximant(pos_b);
            }
            u_b = solverB.solve(rhs_b);

            iter++;
            // write solution
            nameA = "solA-" + std::to_string(iter);
            nameB = "solB-" + std::to_string(iter);
            out.writeDoubleArray(nameA, u_a);
            out.writeDoubleArray(nameB, u_b);
        }

        out.close();
        }

    return 0;
}