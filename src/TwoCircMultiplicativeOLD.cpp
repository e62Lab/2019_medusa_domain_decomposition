//
// Created by blaz on 18.10.2019.

// Multiplicative Schwartz


#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include "medusa/bits/approximations/WLS.hpp"
#include <string>
#include <cmath>


using namespace mm;
int main() {

    double dx = 0.001;
    // number of nodes used in wls approximation other domain value
    int na = 4;
    double radius = 0.5;
    int iterations = 31;
    int n = 9; // support size
    double alpha = 1/dx; // decay coefficient for weighted approximations

    Timer timer;
    timer.addCheckPoint("Begin");

    // Construct domain A and B, in this case they are two offset squares
    BallShape<Vec2d> A(0.0, radius);
    BallShape<Vec2d> B({0.75, 0.0}, radius);

    // Construct General Fill
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);

    // Construct Domain discretizaitons
    DomainDiscretization<Vec2d> domainA = A.discretizeBoundaryWithStep(dx);
    DomainDiscretization<Vec2d> domainB = B.discretizeBoundaryWithStep(dx);

    // Obtain boundary nodes in neighboring domains (indices)
    // This is done at this point, because there are only boundary nodes in both A in B at this point
    auto boundAinB = domainA.positions().filter([=](const Vec2d &p) { return B.contains(p); });
    auto boundBinA = domainB.positions().filter([=](const Vec2d &p) { return A.contains(p); });

    // Fill the inside of domains
    domainA.fill(fill_engine, dx);
    domainB.fill(fill_engine, dx);

    // Construct KDTrees for point lookup when adjusting boundary conditions
    KDTree<Vec2d> treeA(domainA.positions());
    KDTree<Vec2d> treeB(domainB.positions());

    // Output data
    std::string name = "../../postprocess/process/Data/TwoCircMult.h5";
    HDF out(name, HDF::DESTROY);
    out.writeDomain("domainA", domainA);
    out.writeDomain("domainB", domainB);
    out.writeIntAttribute("iterations", iterations);

    int Na = domainA.size();
    int Nb = domainB.size();
    int Nainb = boundAinB.size();
    int Nbina = boundBinA.size();

    std::cout << Nbina << " " << Nainb << std::endl;
    // find support
    domainA.findSupport(FindClosest(n));
    domainB.findSupport(FindClosest(n));

    // construct engine for weights
    WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest> wls({n, 30});

    // construct engine for value approximation
    WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest> appr(1, 100.0);

    // vector to store support nodes for approximation
//    Eigen::VectorXd appvals(na);

    // store weights for rhs
    Eigen::VectorXd weights(na);

    // maps
    Eigen::VectorXd AindMap(Nainb);
    Eigen::VectorXd BindMap(Nbina);

    // index support and position storage
    std::vector<std::vector<int>> AinBindex(Nainb);
    std::vector<Range<Vec2d>> AinBpos(Nainb);

    // index support and position storage
    std::vector<std::vector<int>> BinAindex(Nbina);
    std::vector<Range<Vec2d>> BinApos(Nbina);

    // construct weight matrices and both rhsides
    Eigen::SparseMatrix<double, Eigen::RowMajor> Ma(Na, Na);
    Eigen::SparseMatrix<double, Eigen::RowMajor> Mb(Nb, Nb);
    Eigen::VectorXd rhs_a(Na);
    Eigen::VectorXd rhs_afix(Na); // The fixed part of rhs_a, does not change from iteration to iteration
    Eigen::VectorXd rhs_b(Nb); // note, this initializes the array to zeros
    Eigen::VectorXd rhs_bfix(Nb); // The fixed part of rhs_b, does not change from iteration to iteration

    // sparse matrices for rhs approximations
    Eigen::SparseMatrix<double, Eigen::RowMajor> Sa(Nb, Na); // To set rhs for A, rhs_a = rhs_afixed + Sa*u_b
    Eigen::SparseMatrix<double, Eigen::RowMajor> Sb(Na, Nb); // To set rhs for A, rhs_b = rhs_bfixed + Sb*u_a

    // shapes
    auto storageA = domainA.computeShapes<sh::lap>(wls);
    auto storageB = domainB.computeShapes<sh::lap>(wls);

    // operators
    // Make sure the operators write into fixed rhs part
    // The parts of rhs that change are handled manually
    auto op_A = storageA.implicitOperators(Ma, rhs_afix);
    auto op_B = storageB.implicitOperators(Mb, rhs_bfix);

    // solvers
    Eigen::BiCGSTAB<decltype(Ma), Eigen::IncompleteLUT<double>> solverA;
    Eigen::BiCGSTAB<decltype(Mb), Eigen::IncompleteLUT<double>> solverB;

    // fill the matrices
    for (int i : domainA.interior()) {
        auto pos = domainA.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_A.lap(i) = -std::exp(-x+2*y)*(std::sin(x)*(4*std::sin(y)-3*std::cos(y))+2*std::cos(y)*std::cos(x));
    }
    // set boundary condition, this will be for fixed every iteration
    for (int i : domainA.boundary()) {
        auto pos = domainA.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_A.value(i) = std::exp(-x+2*y)*std::sin(x)*std::cos(y);
    }
    for (int i : domainB.interior()) {
        auto pos = domainB.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_B.lap(i) = -std::exp(-x+2*y)*(std::sin(x)*(4*std::sin(y)-3*std::cos(y))+2*std::cos(y)*std::cos(x));
    }
    for (int i : domainB.boundary()) {
        auto pos = domainB.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_B.value(i) = std::exp(-x+2*y)*std::sin(x)*std::cos(y);
    }
    // fill the matrix and store data for quick access
    int j = 0;
    for (int i : boundAinB) {
        // We have to be careful when setting these values in matrix M, as we will be changing the RHS a later to fix BC
        Ma.coeffRef(i, i) = 1.0; // to make this easier we will make all of the coeffs ones
        rhs_a(i) = 1.0; // Because in real use case, we do not know this boundary value exactly!

        Vec2d pos_a = domainA.pos(i);

        // store data & create index mapping
        auto nearest_in_B = treeB.query(pos_a, na); // std::tie of Range<int> and Range<double>
        auto[ib, r2b] = nearest_in_B; // structured binding declaration, todo learn how this works

        Range<Vec2d> appsupport(na);
        double sum = 0;

        for(int k = 0; k < na; ++k){
            weights[k] = std::exp(-alpha*r2b[i]);
            appsupport[k] = domainB.pos(ib[k]); // get positions
        }


        for(int k = 0; k < na; ++k){
            appsupport[k] = domainB.pos(ib[k]); // get positions
        }

        // store
        AinBindex[j] = ib;
        AinBpos[j] = appsupport;

        // index map for natural numbers -> boundAinB indexes
        AindMap[j] = i;
        j++;
    }

    j = 0;
    for (int i : boundBinA) {
        // We have to be careful when setting these values in matrix M, as we will be changing the RHS a later to fix BC
        Mb.coeffRef(i, i) = 1.0; // to make this easier we will make all of the coeffs ones
        rhs_b(i) = 1.0; // Because in real use case, we do not know this boundary value exactly!
        Vec2d pos_b = domainA.pos(i);

        // store data & create index mapping
        auto nearest_in_A = treeA.query(pos_b, na); // std::tie of Range<int> and Range<double>
        auto[ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this works

        Range<Vec2d> appsupport(na);
        for(int k = 0; k < na; ++k){
            appsupport[k] = domainA.pos(ia[k]);
        }

        // store
        BinAindex[j] = ia;
        BinApos[j] = appsupport;

        // index map for natural numbers -> boundBinA indexes
        BindMap[j] = i;
        j++;
    }

    // compute matrices
    solverA.compute(Ma);
    solverB.compute(Mb);

    // init complete
    timer.addCheckPoint("Init");

    // Intermediate output
    out.writeSparseMatrix("Ma", Ma);
    out.writeSparseMatrix("Mb", Mb);
//    out.writeIntArray("boundAinB", boundAinB);
//    out.writeIntArray("boundBinA", boundBinA);

    // start iterating
    std::cout << "Iteration no. " << 0 << "\n";
    std::cout << "Solving domain A" << "\n";
    // solve equation on subdomain A, with initial border
    ScalarFieldd u_a = solverA.solve(rhs_a);

    std::cout << "Solving domain B" << "\n";
    // solve equation on subdomain B, with initial border
    ScalarFieldd u_b = solverB.solve(rhs_b);

    // time initial solution
    timer.addCheckPoint("Sol-0");

    // write solution
    out.writeDoubleArray("solA-0", u_a);
    out.writeDoubleArray("solB-0", u_b);

    for (int iter = 1; iter < iterations; iter++) {
        std::cout << "Iteration no. " << iter << "\n";

        // Adjust both boundaries
        std::cout << "Adjusting boundary condition for domain A" << "\n";
        for (int i = 0; i < Nainb;i++) {
            // get approx support indexes
            auto ib = AinBindex[i];
            // get index of the node being approximated
            int index = AindMap[i];

            // get position
            Vec2d pos_a = domainA.pos(index);

            // set values in support nodes
            for (int j = 0; j < na; j++) {
                appvals[j] = u_b(ib[j]);
            }
            auto approximant = appr.getApproximant(pos_a, AinBpos[i], appvals);
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
            rhs_a(index) = approximant(pos_a);
        }

        std::cout << "Adjusting boundary condition for domain B" << "\n";
        for (int i = 0; i < Nbina; i++) {
            // get indexes
            auto ia = BinAindex[i];
            // get node index
            int index = BindMap[i];

            // get pos
            Vec2d pos_b = domainB.pos(index);
            // find nearest in domain A

            for (int j = 0; j < na; j++) {
                appvals[j] = u_a(ia[j]);
            }
            auto approximant = appr.getApproximant(pos_b, BinApos[i], appvals);
            // approximate value at i-th node in B domain with WLS from the 4 nearest values in the A domain
            rhs_b(index) = approximant(pos_b);
        }

        // Solve both domains
        std::cout << "Solving domain A" << "\n";
        u_a = solverA.solve(rhs_a);
        std::cout << "Solving domain B" << "\n";
        u_b = solverB.solve(rhs_b);

        // time the iteration
        timer.addCheckPoint("Sol-"+std::to_string(iter));

        // output
        std::string nameA = "solA-" + std::to_string(iter);
        std::string nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);
    }


    // timer out file

    std::cout << timer << std::endl;
    out.writeTimer("timer", timer);
    out.close();

    return 0;
}
