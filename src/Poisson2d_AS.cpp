//
// Created by blaz on 10.10.2019.
// Domain decomposition implementation, additive schwartz method, different
// implementation of approximation boundary nodes. This one uses the closest node from the neighboring domain

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>

using namespace mm;

int main(){

    double dx = 0.01;

    // Construct domain A and B, in this case they are two offset squares
    BoxShape<Vec2d> A(0.0, {0.6, 1.0});
    BoxShape<Vec2d> B({0.4, 0.0}, {1.0, 1.0});

    // Fill both the uniformly fill both the domains
    DomainDiscretization<Vec2d> domainA = A.discretizeWithStep(dx);
    DomainDiscretization<Vec2d> domainB = B.discretizeWithStep(dx);

    // Construct KDTrees for point lookup when adjusting boundary conditions
    KDTree<Vec2d> treeA(domainA.positions());
    KDTree<Vec2d> treeB(domainB.positions());

    // Output data
    HDF out("../../postprocess/process/Data/Poisson2d_AS-Closest.h5", HDF::DESTROY);
    out.writeDomain("domainA", domainA);
    out.writeDomain("domainB", domainB);

    int Na = domainA.size();
    int Nb = domainB.size();

    int n = 9; // support size

    // find support
    domainA.findSupport(FindClosest(n));
    domainB.findSupport(FindClosest(n));

    // construct engine
    WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest> wls({n, 30});

    // construct weight matrices and both rhsides
    Eigen::SparseMatrix<double, Eigen::RowMajor> Ma(Na, Na);
    Eigen::SparseMatrix<double, Eigen::RowMajor> Mb(Nb, Nb);
    Eigen::VectorXd rhs_a(Na);
    Eigen::VectorXd rhs_b(Nb);

    // shapes
    auto storageA = domainA.computeShapes<sh::lap>(wls);
    auto storageB = domainB.computeShapes<sh::lap>(wls);

    // operators
    auto op_A = storageA.implicitOperators(Ma, rhs_a);
    auto op_B = storageB.implicitOperators(Mb, rhs_b);

    // solvers
    Eigen::BiCGSTAB<decltype(Ma), Eigen::IncompleteLUT<double>> solverA;
    Eigen::BiCGSTAB<decltype(Mb), Eigen::IncompleteLUT<double>> solverB;


    double a = -10;
    // fill the matrices, lap u = 1
    for (int i : domainA.interior()){
        op_A.lap(i) = a;
    }
    // set boundary condition, this will be fixed every iteration
    for (int i : domainA.boundary()){
        op_A.value(i) = 0;
    }

    for (int i : domainB.interior()){
        op_B.lap(i) = a;
    }
    for (int i : domainB.boundary()){
        op_B.value(i) = 0;
    }

    // compute matrices
    solverA.compute(Ma);
    solverB.compute(Mb);


    int iterations = 99;
    std::cout << "Iteration no. " << 0 << "\n";
    std::cout << "Solving domain A" << "\n";

    // solve equation on subdomain A, with initial border
    ScalarFieldd u_a = solverA.solve(rhs_a);

    // fix boundary condition on B, the left boundary values are set to approximation from u_a
    std::cout << "Adjusting boundary condition for domain B" << "\n";
    for (int i : domainB.types() == -1){
        Vec2d pos_b = domainB.pos(i);
//        std::cout << "Position of node " << i << " in B: " << pos_b << "\n";
        // find nearest in domain A
        auto nearest_in_A = treeA.query(pos_b, 1); // std::tie of Range<int> and Range<double>
        auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this works
//        std::cout << "Position of node " << ia[0] << " in A: " << domainA.pos(ia[0]) << "\n";
        rhs_b(i) = u_a(ia[0]);
    }

    // solve B
    std::cout << "Solving domain B" << "\n\n";

    ScalarFieldd u_b = solverB.solve(rhs_b);
    // write solution
    out.writeDoubleArray("solA-0", u_a);
    out.writeDoubleArray("solB-0", u_b);


    // iterate
    for(int iter = 1; iter < iterations; iter++){
        std::cout << "Iteration no. " << iter << "\n";
        std::cout << "Adjusting boundary condition for domain A" << "\n";
        std::cout << "Solving domain A" << "\n";
        for (int i : domainA.types() == -2){
            Vec2d pos_a = domainA.pos(i);
    //        std::cout << "Position of node " << i << " in A: " << pos_a << "\n";
            // find nearest in domain A
            auto nearest_in_B = treeB.query(pos_a, 1); // std::tie of Range<int> and Range<double>
            auto [ib, r2b] = nearest_in_B; // structured binding declaration, todo learn how this works
    //        std::cout << "Position of node " << ia[0] << " in A: " << domainA.pos(ia[0]) << "\n";
            rhs_a(i) = u_b(ib[0]);
        }
        u_a = solverA.solve(rhs_a);

        std::string nameA = "solA-" + std::to_string(iter);
        std::string nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);

        std::cout << "Adjusting boundary condition for domain B" << "\n";
        std::cout << "Solving domain B" << "\n\n";

        for (int i : domainB.types() == -1){
            Vec2d pos_b = domainB.pos(i);
//        std::cout << "Position of node " << i << " in B: " << pos_b << "\n";
            // find nearest in domain A
            auto nearest_in_A = treeA.query(pos_b, 1); // std::tie of Range<int> and Range<double>
            auto [ia, r2a] = nearest_in_A; // structured binding declaration, todo learn how this works
//        std::cout << "Position of node " << ia[0] << " in A: " << domainA.pos(ia[0]) << "\n";
            rhs_b(i) = u_a(ia[0]);
        }
        u_b = solverB.solve(rhs_b);

        iter++;
        // write solution
        nameA = "solA-" + std::to_string(iter);
        nameB = "solB-" + std::to_string(iter);
        out.writeDoubleArray(nameA, u_a);
        out.writeDoubleArray(nameB, u_b);
    }

    return 0;
}



