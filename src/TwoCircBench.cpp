//
// Created by blaz on 18.10.2019.
// Benchmark for two circles

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <string>
#include <cmath>


using namespace mm;
int main(){

    double dx = 0.001;
    double radius = 0.5;
    int n = 9; // support size


    // Construct timer
   Timer timer;
   timer.addCheckPoint("Begin");


    // Construct domain A and B, in this case they are two offset squares
    BallShape<Vec2d> A(0.0, radius);
    BallShape<Vec2d> B({0.75, 0.0}, radius);

    // Construct General Fill
    GeneralFill<Vec2d> fill_engine;
    fill_engine.seed(1);

    // Construct Domain discretizaitons
    DomainDiscretization<Vec2d> domain = (A + B).discretizeBoundaryWithStep(dx);

    // Fill the inside of domains
    domain.fill(fill_engine, dx);

    // Output data
    std::string name = "../../postprocess/process/Data/TwoCircBench.h5";
    HDF out(name, HDF::DESTROY);
    out.writeDomain("domain", domain);

    int N = domain.size();

    // find support
    domain.findSupport(FindClosest(n));

    // construct engine for weights
    WLS<Gaussians<Vec2d>, NoWeight<Vec2d>, ScaleToFarthest> wls({n, 30});

    // construct weight matrices and both rhsides
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N);

    // shapes
    auto storage = domain.computeShapes<sh::lap>(wls);

    // operators
    auto op = storage.implicitOperators(M, rhs);

    // solvers
    Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;

    // fill the matrices
    for (int i : domain.interior()){
        auto pos = domain.pos(i);
        double x = pos[0];
        double y = pos[1];
        op.lap(i) = -std::exp(-x+2*y)*(std::sin(x)*(4*std::sin(y)-3*std::cos(y))+2*std::cos(y)*std::cos(x));

    }
    // set boundary condition, this will be for fixed every iteration
    for (int i : domain.boundary()){
        auto pos = domain.pos(i);
        double x = pos[0];
        double y = pos[1];
        op.value(i) = std::exp(-x+2*y)*std::sin(x)*std::cos(y);
    }
    solver.compute(M);
    timer.addCheckPoint("Init");

    ScalarFieldd u = solver.solve(rhs);

    timer.addCheckPoint("Solve");
    // write solution
    std::cout << timer << std::endl;
    out.writeTimer("timer", timer);
    out.writeDoubleArray("sol", u);
//    out.writeDoubleAttribute();
    out.close();

    return 0;
}
