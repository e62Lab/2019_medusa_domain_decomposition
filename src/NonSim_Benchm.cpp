#include <medusa/Medusa_fwd.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <string>
#include <cmath>


using namespace mm;
int main(){

    double dx = 0.01;
    double squarewidth = 1.0;
    int n = 9; // support size

    BoxShape <Vec2d> A(0.0, squarewidth);

    // Construct General Fill
    GeneralFill <Vec2d> fill_engine;
    fill_engine.seed(1);

    // Construct Domain discretizaitons
    DomainDiscretization <Vec2d> domainA = A.discretizeBoundaryWithStep(dx);

    domainA.fill(fill_engine, dx);

    // Output data
    std::string name =
            "../../postprocess/process/Data/NonSimBench-WLS.h5";
    HDF out(name, HDF::DESTROY);
    out.writeDomain("domainA", domainA);

    int Na = domainA.size();

    // find support
    domainA.findSupport(FindClosest(n));

    // construct engine for weights
    WLS<Gaussians < Vec2d>, NoWeight<Vec2d>, ScaleToFarthest > wls({n, 30});

    // construct weight matrices and both rhsides
    Eigen::SparseMatrix<double, Eigen::RowMajor> Ma(Na, Na);
    Eigen::VectorXd rhs_a(Na);

    // shapes
    auto storageA = domainA.computeShapes<sh::lap>(wls);

    // operators
    auto op_A = storageA.implicitOperators(Ma, rhs_a);

    // solvers
    Eigen::BiCGSTAB<decltype(Ma), Eigen::IncompleteLUT<double>> solverA;

    // fill the matrices
    for (int i : domainA.interior()) {
        auto pos = domainA.pos(i);
        double x = pos[0];
        double y = pos[1];
        op_A.lap(i) = -std::sin(3 / 2 * PI * x) * std::exp(-x - y) *
                      ((4 * PI * PI - 1) * std::sin(2 * PI * y) + 4 * PI * std::cos(2 * PI * y))
                      - 1 / 4 * std::sin(2 * PI * y) * std::exp(-x - y) *
                        ((9 * PI * PI - 4) * std::sin(3 / 2 * PI * x) + 12 * PI * std::cos(3 / 2 * PI * x));
    }
    // set boundary condition, this will be fixed every iteration
    for (int i : domainA.types() == -1) {
        op_A.value(i) = 0;
    }
    for (int i : domainA.types() == -2) {
        op_A.value(i) = 1;
    }
    for (int i : domainA.types() == -3) {
        auto pos = domainA.pos(i);
        op_A.value(i) = pos[0];
    }
    for (int i : domainA.types() == -4) {
        auto pos = domainA.pos(i);
        op_A.value(i) = pos[0];
    }
    solverA.compute(Ma);

    // start iterating
    std::cout << "Iteration no. " << 0 << "\n";
    std::cout << "Solving domain A" << "\n";

    // solve equation on subdomain A, with initial border
    ScalarFieldd u_a = solverA.solve(rhs_a);
    out.writeDoubleArray("sol", u_a);
    return 0;
}
