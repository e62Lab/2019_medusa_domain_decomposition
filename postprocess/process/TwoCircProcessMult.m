name = "Data/TwoCircMult.h5";
a_pos = h5read(name, "/domainA/pos");
b_pos = h5read(name, "/domainB/pos");
iterations = h5readatt(name, '/' ,'iterations');
radius = 0.5;

analytical = @(x, y) exp(-x+2*y).*sin(x).*cos(y);

analyticalA = analytical(a_pos(:, 1), a_pos(:,2));
analyticalB = analytical(b_pos(:, 1), b_pos(:,2));

% abserr = ones(1, iterations);
% % Aerr = [];
% % Berr = [];
% Aerrsum = ones(1, iterations);
% Berrsum = ones(1, iterations);
% 
% for i=0:iterations-1
%     %we start with B being better than A, consider whole B and only part of
%     %A with x component smaller than half - gap.
%     s = num2str(i);
%     nameA = "/solA-" + s;
%     nameB = "/solB-" + s;
%     a_sol = h5read(name, nameA);
%     b_sol = h5read(name, nameB);
%        
%     % error in each individual circle
% %     Aerr = [Aerr, [abs(a_sol - analyticalA)]]; % in each node
% %     Berr = [Berr, [abs(b_sol - analyticalB)]];
%     
%     % absolute err
%     Aerrsum(i+1) = sum(abs(a_sol - analyticalA))/length(a_sol);
%     Berrsum(i+1) = sum(abs(b_sol - analyticalB))/length(b_sol);
% end

% times process
h = h5info(name);
groups = h.Groups();

times = ones(1, iterations);
init_time_iter = groups(3).Attributes(1).Value;
for i=2:length(groups(3).Attributes())-1
    times(i-1) = groups(3).Attributes(i).Value;
end

% save('../plot/Data/CMabserr.mat', 'abserr')
% save('../plot/Data/CM_Aerr.mat', 'Aerr')
% save('../plot/Data/CM_Berr.mat', 'Berr')
% save('../plot/Data/CM_Aerrsum.mat', 'Aerrsum')
% save('../plot/Data/CM_Berrsum.mat', 'Berrsum')
% save('../plot/Data/CM_Apos.mat', 'a_pos')
% save('../plot/Data/CM_Bpos.mat', 'b_pos')
save('../plot/Data/CM_itertimes', 'times')
save('../plot/Data/CM_inittimes', 'init_time_iter')
% save('../plot/Data/CM_iterations', 'iterations')

% benchmark solution
name = "Data/TwoCircBench.h5";
pos = h5read(name, "/domain/pos");
% sol = h5read(name, "/sol");
% b_an = analytical(pos(:, 1), pos(:,2));
% bencherr = sum(abs(b_an - sol))/length(sol);
% bench = ones(1, length(Aerrsum))*bencherr;

h = h5info(name);
groups = h.Groups();
timer_atts = groups(2).Attributes();
init_time_bench = timer_atts(1).Value;
sol_time_bench = timer_atts(2).Value;

save('../plot/Data/CM_sol_time.mat', 'sol_time_bench')
save('../plot/Data/CM_init_time.mat', 'init_time_bench')
% save('../plot/Data/CMbench.mat', 'bench')

