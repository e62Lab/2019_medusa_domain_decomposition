name = "Data/TwoCirc-WLS.h5";
a_pos = h5read(name, "/domainA/pos");
b_pos = h5read(name, "/domainB/pos");
iterations = h5readatt(name, '/' ,'iterations');
radius = 0.5;

analytical = @(x, y) exp(-x+2*y).*sin(x).*cos(y);

analyticalA = analytical(a_pos(:, 1), a_pos(:,2));
analyticalB = analytical(b_pos(:, 1), b_pos(:,2));

a_mask = (a_pos(:, 1)).^2 + a_pos(:, 2).^2 <= 0.5^2; 
b_mask = (a_pos(:, 1) - 1).^2 + a_pos(:, 2).^2 <= 0.5^2;

abpos = [a_pos; [(b_pos(b_mask, 1)), (b_pos(b_mask, 2))]]; % A has better approximation than B
analytical_ab = analytical(abpos(:,1), abpos(:,2));

bapos = [[(a_pos(a_mask, 1)), (a_pos(a_mask, 2))]; b_pos]; % other way around
analytical_ba = analytical(bapos(:,1), bapos(:,2));

abserr = ones(1, iterations);
baerr = [];
aberr = [];
Aerr = [];
Berr = [];
Aerrsum = ones(1, iterations);
Berrsum = ones(1, iterations);

for i=0:iterations-1
    i
    if rem(i, 2) == 0
        %we start with B being better than A, consider whole B and only part of
        %A with x component smaller than half - gap.
        s = num2str(i);
        nameA = "/solA-" + s;
        nameB = "/solB-" + s;
        a_sol = h5read(name, nameA);
        b_sol = h5read(name, nameB);
        basol = [a_sol(a_mask); b_sol];
%         absol = [a_sol; b_sol(b_mask)];
        baerr = [[baerr], [abs(basol - analytical_ba)]];
        abserr(i + 1) = sum(abs(basol - analytical_ba))/length(basol);      
    end
   
    if rem(i, 2) == 1
        i
        s = num2str(i);
        nameA = "/solA-" + s;
        nameB = "/solB-" + s;
        a_sol = h5read(name, nameA);
        b_sol = h5read(name, nameB);
        absol = [a_sol; b_sol(b_mask)];
%         basol = [a_sol(a_mask); b_sol];
        aberr = [[aberr], [abs(absol - analytical_ab)]];
        abserr(i + 1) = sum(abs(absol - analytical_ab))/length(absol);
    end
       
    % error in each individual circle
    Aerr = [Aerr, [abs(a_sol - analyticalA)]]; % in each node
    Berr = [Berr, [abs(b_sol - analyticalB)]];
    
    % absolute err
    Aerrsum(i+1) = sum(abs(a_sol - analyticalA))/length(a_sol);
    Berrsum(i+1) = sum(abs(b_sol - analyticalB))/length(b_sol);
end

save('../plot/Data/CIRCabserr.mat', 'abserr')
save('../plot/Data/CIRC_Aerr.mat', 'Aerr')
save('../plot/Data/CIRC_Berr.mat', 'Berr')
save('../plot/Data/CIRC_Aerrsum.mat', 'Aerrsum')
save('../plot/Data/CIRC_Berrsum.mat', 'Berrsum')
save('../plot/Data/CIRC_Apos.mat', 'a_pos')
save('../plot/Data/CIRC_Bpos.mat', 'b_pos')


% benchmark solution
name = "Data/TwoCircBench.h5";
pos = h5read(name, "/domain/pos");
sol = h5read(name, "/sol");
b_an = analytical(pos(:, 1), pos(:,2));
bencherr = sum(abs(b_an - sol))/length(sol);
bench = ones(1, length(Aerrsum))*bencherr;

h = h5info(name);
groups = h.Groups();
timer_atts = groups(2).Attributes();
init_time = timer_atts(1).Value;
sol_time = timer_atts(2).Value;

save('../plot/Data/CIRC_sol_time.mat', 'sol_time')
save('../plot/Data/CIRC_init_time.mat', 'init_time')
save('../plot/Data/CIRCbench.mat', 'bench')

