clear all;
close all;

bandwidths = linspace(5, 45, 9);
name = "NonSim-WLS-5.h5";
iter = h5readatt(name,'/' ,'iterations');
abserrors = ones(length(bandwidths) + 1, iter); %Bandwidths + 1 for benchmark solution
j = 0;

for width=bandwidths
    j = j + 1;
    name = "NonSim-WLS-" + num2str(width) + ".h5";
    a_pos = h5read(name, "/domainA/pos");
    b_pos = h5read(name, "/domainB/pos");

    gap = width/100; % part of a that reaches over the half of the whole domain
    iter = h5readatt(name,'/' ,'iterations');
    analytical  = @(x, y) exp(-(x + y)).*sin(3*pi*x/2).*sin(2* pi* y) + x;
    
     a_mask = a_pos(:, 1) <= 0.5-(width/100);
     b_mask = b_pos(:, 1) >= 0.5+(width/100);

     abpos = [a_pos; [(b_pos(b_mask, 1)), (b_pos(b_mask, 2))]]; % A has better approximation than B
     analytical_ab = analytical(abpos(:,1), abpos(:,2));

     bapos = [[(a_pos(a_mask, 1)), (a_pos(a_mask, 2))]; b_pos]; % other way around
     analytical_ba = analytical(bapos(:,1), bapos(:,2));

    avgerr = ones(iter, 1);
    for i=0:iter-1
        if rem(i, 2) == 0
            %we start with B being better than A, consider whole B and only part of
            %A with x component smaller than half - gap.
            s = num2str(i);
            nameA = "/solA-" + s;
            nameB = "/solB-" + s;
            a_sol = h5read(name, nameA);
            b_sol = h5read(name, nameB);
            basol = [a_sol(a_mask); b_sol];
            avgerr(i + 1) = sum(abs(basol - analytical_ba))/length(basol);
        end

        if rem(i, 2) == 1
            s = num2str(i);
            nameA = "/solA-" + s;
            nameB = "/solB-" + s;
            a_sol = h5read(name, nameA);
            b_sol = h5read(name, nameB);
            absol = [a_sol; b_sol(b_mask)];
            avgerr(i + 1) = sum(abs(absol - analytical_ab))/length(absol);
        end
        abserrors(j, :) = avgerr.';
    end  
end

% Benchmark error
bench = h5read("NonSimBench-WLS.h5", "/sol");
pos = h5read("NonSimBench-WLS.h5", "/domainA/pos");
analytical_bench = analytical(pos(:,1), pos(:,2));
% scatter3(pos(:,1), pos(:,2), bench)
abserrors(length(abserrors(:,1)), :) = sum(abs(bench - analytical_bench)/length(basol));


% save data
save('../plot/NSbandwidths.mat', 'bandwidths')
save('../plot/NSabserrors.mat', 'abserrors')
save('../plot/NSiterations.mat', 'iter')

