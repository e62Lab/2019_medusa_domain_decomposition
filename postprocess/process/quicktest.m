b_sol = h5read(name, "/solB-30");
a_sol = h5read(name, "/solA-30");
f1 = setfig('b1', [2*720 2*720])
scatter3(a_pos(:, 1), a_pos(:, 2), a_sol, 'b.', 'DisplayName', 'Domena A');
% scatter3(a_pos(:, 1), a_pos(:, 2), a_sol, 'b.', 'DisplayName', 'Domena A');
hold on
scatter3(b_pos(:, 1), b_pos(:, 2), b_sol ,'r', 'DisplayName', 'Domena B');
% scatter3(b_pos(:, 1), b_pos(:, 2), analytical(b_pos(:, 1), b_pos(:, 2)), 'k.');
% scatter3(a_pos(:, 1), a_pos(:, 2), analytical(a_pos(:, 1), a_pos(:, 2)), 'k.');
view(20, 25)
exportf(f1, 'TwoCirc30it.png');
% Ma = h5read(name, "/Ma");
% Mb = h5read(name, "/Mb");
% 
% A_B = h5read(name, "/boundAinB");
% B_A = h5read(name, "/boundBinA");
% 
% for i=A_B
%     Ma(i + 1, 3)
% end