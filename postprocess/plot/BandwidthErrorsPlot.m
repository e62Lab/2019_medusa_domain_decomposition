clear all;
close all;


load('NSbandwidths.mat')
load('NSiterations.mat')
load('NSabserrors.mat')

% load('bandwidths.mat')
% load('iterations.mat')
% load('abserrors.mat')

f1 = setfig('b1', [2*720 2*720]);
hold on
i = 0;
for width=bandwidths;
    i = i + 1;
    plot(abserrors(i, :), '-x')
    xlabel("Iterations");
    ylabel("Absolute error");
    set(gca, 'YScale', 'log');  
%     xlim([0 41])
%     ylim([0.1 0.2]) 
end
plot(abserrors(length(bandwidths) + 1, :), 'k-')
xlabel("Iterations");
ylabel("Absolute error");
set(gca, 'YScale', 'log');
legend('width  = $0.05$', 'width  = $0.10$' , 'width  = $0.15$' , 'width  = $0.20$', 'width  = $0.25$','width  = $0.30$', 'width  = $0.35$' , 'width  = $0.40$' , 'width  = $0.45$', 'benchmark')
% exportf(f1, 'NSbwdths.png')
