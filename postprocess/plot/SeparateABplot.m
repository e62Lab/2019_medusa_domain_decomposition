clear all;
close all;

% not the best script use with caution

load("Data/CIRC_Aerr")
load("Data/CIRC_Aerrsum")
load("Data/CIRC_Berr")
load("Data/CIRC_Berrsum")
load("Data/CIRC_Apos")
load("Data/CIRC_Bpos")
load("Data/iterations")

f_1 = setfig('a3', [720 720])
title("Napaka domena A")
s1 = plot(linspace(1, 31, 31), Aerrsum, 'ko-')
xlabel("iterations")
ylabel("abs error")

f_2 = setfig('a4', [720 720])
title("Napaka domena B")
s1 = plot(linspace(1, 31, 31), Berrsum, 'ko-')
xlabel("iterations")
ylabel("abs error")


a_sol = Aerr(:,1);
b_sol = Aerr(:,1);

f1 = setfig('b1', [720 720]);
title("A, Napaka po iteraciji 0");
s1 = scatter(a_pos(:, 1), a_pos(:, 2), 15, a_sol, 'filled', 'DisplayName', 'Domena A');
axis equal

% f2 = setfig('b2', [720 720]);
% title("B, Napaka po iteraciji 0");
% s2 = scatter(b_pos(:, 1), b_pos(:, 2),  15, b_sol, 'filled', 'DisplayName', 'Domena A');
% axis equal
% view(10, 25)
% zlim([0 1])

% link data
s1.ZDataSource = 'a_sol';
% s2.ZDataSource = 'b_sol';

maxiter = 31;
i = 0;
while true
    % wait on input
    % then decide what to do with it
    k = waitforbuttonpress;
    w = double(get(gcf, 'CurrentCharacter'));
    if w == 29 %right arrow
        i = mod(i+1, maxiter) ;
    elseif w == 28 %left arrow
        i = mod(i-1, maxiter);
    elseif w== 27 %escape
        break
    end
     
    s = num2str(i);
    title("A, after iteration: " + s);
%     title("B, after iteration:" + s);
    a_sol = Aerr(:, i + 1)
%     b_sol = Berr(:, i + 1);    
    refreshdata
    colorbar
    drawnow
    
%     if i == 30
%         exportf(f2, 'NS30it.png');
%     end
end
close(f2) 