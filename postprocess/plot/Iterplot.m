% name = "../process/Data/NonSim-WLS-15.h5";
% name = "../process/Data/TwoCirc-WLS.h5"
name = "../process/Data/TwoCircMult.h5"

iterations = h5readatt(name, '/', 'iterations');

h = h5info(name);
a_pos = h5read(name, "/domainA/pos");
b_pos = h5read(name, "/domainB/pos");

a_sol = h5read(name, "/solA-0");
b_sol = h5read(name, "/solB-0");

f2 = setfig('a1', [720*2 720*2]);
title("Domeni $\Omega_A$ in $\Omega_B$ po iteraciji 0");
s1 = scatter3(a_pos(:, 1), a_pos(:, 2), a_sol, 'b.', 'DisplayName', 'Domena A');
s2 = scatter3(b_pos(:, 1), b_pos(:, 2), b_sol ,'r', 'DisplayName', 'Domena B');
view(10, 25)
% zlim([0 1])

% link data
s1.ZDataSource = 'a_sol';
s2.ZDataSource = 'b_sol';

maxiter = iterations;
i = 0;
while true
    % wait on input
    % then decide what to do with it
    k = waitforbuttonpress;
    w = double(get(gcf, 'CurrentCharacter'));
    if w == 29 %right arrow
        i = mod(i+1, maxiter) ;
    elseif w == 28 %left arrow
        i = mod(i-1, maxiter);
    elseif w== 27 %escape
        break
    end
        
    % refresh data
    s = num2str(i);
    title("Domeni $\Omega_A$ in $\Omega_B$ po iteraciji " + s);
    nameA = "/solA-" + s;
    nameB = "/solB-" + s;
    a_sol = h5read(name, nameA);
    b_sol = h5read(name, nameB);    
    refreshdata
    drawnow
    
    if i == 30
        exportf(f2, 'NS30it.png');
    end
end
close(f2) 

