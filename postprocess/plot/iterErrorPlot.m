clear all;
close all;

load("NSbapos.mat");
load("NSabpos.mat");
load("NSbaerr.mat");
load("NSaberr.mat");
load("NSavgerr.mat");

% load("bapos.mat");
% load("abpos.mat");
% load("baerr.mat");
% load("aberr.mat");
% load("avgerr.mat");

xpos = bapos(:, 1);
ypos = bapos(:, 2);
err = baerr(:, 1);


f2 = setfig('b1', [720 720]);
title("Absolute error  on $\Omega_A$ and $\Omega_B$ after iteration 0");
s1 = scatter(xpos, ypos, 16, err, 'filled' ,  'DisplayName', '$\Omega_A$');
htext = text(0.1, - 0.2, "$\epsilon_a$ = " + num2str(avgerr(1)))
axis equal
colorbar
caxis([0 0.5])
exportf(f2, 'itererrorplot0.png');

% view(-30,10)
% zlim([0 1])

% % link data
s1.CDataSource = 'err';
s1.XDataSource = 'xpos';
s1.YDataSource = 'ypos';

maxiter = 40;
i = 1;
while true
    % wait on input
    % then decide what to do with it
    k = waitforbuttonpress;
    w = double(get(gcf, 'CurrentCharacter'));
    if w == 29 %right arrow
        i = mod(i + 1, maxiter);      
    elseif w == 28 %left arrow
        i = mod(i - 1, maxiter);
    elseif w== 27 %escape
        break
    end
    
    % decide which error to look at
    if rem(i, 2) == 0
        % ba
        xpos = bapos(:, 1);
        ypos = bapos(:, 2);
        err = baerr(:, i / 2 + 1);
    else
        % ab
        xpos = abpos(:, 1);
        ypos = abpos(:, 2);
        err = aberr(:, (i + 1) / 2);
    end
    
    title("Absolute error  on $\Omega_A$ and $\Omega_B$ after iteration " + num2str(i));
    delete(htext);
    htext = text(0.1, - 0.2, "$\epsilon_a$ = " + num2str(avgerr(i + 1)));
    % refresh data
    refreshdata
    drawnow
    exportf(f2, num2str(i), '-png');
end