name = "Poisson2d_AS-WLS.h5";
dist = h5read(name, "/dist");

f1 = setfig('b1', [720 720]);
hist(dist, 30)
title("Oddaljenost prvih \v stirih najbli\v zjih to\v ck")
xlabel("$\delta x$")
ylabel("N")