clear all;
% close all;
% load("avgerr.mat");
load("Data/CIRCabserr.mat");
load("Data/CIRCbench.mat");
benchmark = true;

% make fig
f1 = setfig('a1', [720 720]);
plot(abserr, 'r-x')

% benchmark
if benchmark
    plot(bench, 'k--')
end

xlabel("Iterations");
ylabel("Absolute error");
set(gca, 'YScale', 'log');
title("Absolute error for two circular domains");
% exportf(f1, 'Plots/circErr.png');
% set(gca, 'XScale', 'log')