h = h5info("Poisson2d_AS-Closest.h5");
a_pos = h5read("Poisson2d_AS-Closest.h5", "/domainA/pos");
b_pos = h5read("Poisson2d_AS-Closest.h5", "/domainB/pos");

% Plot domains
f1 = setfig('b1', [720 720]);
title("Domeni $\Omega_1$ in  $\Omega_2$");
scatter(a_pos(:, 1), a_pos(:, 2), 'b.', 'DisplayName', 'Domena A');
scatter(b_pos(:, 1), b_pos(:, 2), 'rs', 'DisplayName', 'Domena B');
xlim([-0.1 1.1])
ylim([-0.1 1.1])
