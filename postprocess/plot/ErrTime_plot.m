clear all;
close all;

% todo load dx
dx = 0.005;

% errors
load("Data/CM_Aerrsum.mat")
load("Data/CM_Berrsum.mat")
load("Data/CMbench.mat")
load("Data/CM_iterations.mat")

% iter times
load("Data/CM_inittimes.mat")
load("Data/CM_itertimes.mat")

% benchmark times
load("Data/CM_init_time.mat")
load("Data/CM_sol_time.mat")

f1 = setfig('b1', [720 2*720])
% Error plot
subplot(2,1,1)
iterspace = linspace(1, 31 , 31);
title("Absolute error, $N=1.5*10^4$")
hold on
plot(iterspace, Aerrsum, 'bo-', 'DisplayName', 'Domain $\Omega_A$')
plot(iterspace, Berrsum, 'ro-', 'DisplayName', 'Domain $\Omega_B$')
plot(iterspace, bench, 'k--', 'DisplayName', 'Benchmark solution')
set(gca, 'YScale', 'log');
xlim([0, 15])
legend
grid
xlabel("iterations")
ylabel("Absolute error")

% Time plot
subplot(2,1,2)
iterspace = linspace(1, 31 , 31);
plot(iterspace, times, 'bo-', 'DisplayName', 'Time for each iteration')
cumtime = cumsum(times);
hold on
plot(iterspace, cumtime, 'ro-', 'DisplayName', 'total time')
plot(iterspace, sol_time_bench*ones(length(iterspace), 1), 'k--', 'DisplayName', 'Benchmark solution')
title("Time complexity, $N=1.2*10^6$")
xlim([0, 15])
legend
grid
xlabel("iterations")
ylabel("Time[s]")

exportf(f1, 'Plots/timecomplexity.png')